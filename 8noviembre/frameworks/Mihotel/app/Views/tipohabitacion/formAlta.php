<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <?php if (!empty($errores)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errores as $field => $error): ?>
                <p><?=$field?>:<?= $error ?></p>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <form action="<?= site_url('tipohabitacion/formAlta')?>" method="post">
        <div class="form-group">
            <?= form_label('ID:', 'id', ['class'=>'col-2'])?>
            <?= form_input('id',set_value('id',''),['class'=>'form_control col-9', 'id'=>'id']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Nombre:', 'nombre', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('nombre',set_value('nombre',''),['class'=>'form_control col-9', 'id'=>'nombre']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Tecnologia:', 'apellido1', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('tecnologia',set_value('tecnologia',''),['class'=>'form_control col-9', 'id'=>'tecnologia']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Capacidad:', 'Capacidad', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('Capacidad',set_value('Capacidad',''),['class'=>'form_control col-9', 'id'=>'Capacidad']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Adultos:', 'Adultos', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('Adultos',set_value('Adultos',''),['class'=>'form_control col-9', 'id'=>'Adultos']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Ninyos:', 'Ninyos', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('Ninyos',set_value('Ninyos',''),['class'=>'form_control col-9', 'id'=>'Ninyos']) ?>
        </div>
        <input type="submit" name="enviar" value="Enviar" />
    </form>
<?= $this->endSection() ?>


