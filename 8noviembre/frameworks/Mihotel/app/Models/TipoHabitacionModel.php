<?php

namespace App\Models;
use CodeIgniter\Model;


class TipoHabitacionModel extends Model {
    
    protected $table = 'tipohabitacion';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    protected $allowedFields = ['nombre','descripcion','tecnologia','capacidad', 'adultos', 'ninyos'];
    
    protected $validationRules = [
        'id',
        'nombre',
        'capacidad'
    ];
    
}
