<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\AlumnosModel;

/**
 * Description of AlumnoController
 *
 * @author jbarrachinab
 */
class AlumnoController extends BaseController {
    
    public function index(){
        $alumnoModel = new AlumnosModel();
        /*echo '<pre>';
        print_r($alumnoModel->findAll());
        echo '</pre>';*/
        $data['titulo'] = 'Listado de Alumnado';
        $data['alumnos'] = $alumnoModel->findAll();
        return view('alumnos/lista',$data);
    }
    
    public function formEdit($id){
        helper('form');
        $alumnoModel = new AlumnosModel();
        $data['alumno'] = $alumnoModel->find($id);
        $data['titulo'] = 'Modificar Alumno/a';
        /*echo '<pre>';
        print_r($data);
        echo '</pre>';*/
        return view('alumnos/formEdit',$data);
    }
    
    public function actualiza($id){
        $alumno = $this->request->getPost();
        /*unset($alumno['botoncito']);
        echo '<pre>';
        print_r($alumno);
        echo '</pre>';*/
        $alumnoModel = new AlumnosModel();
        $alumnoModel->update($id, $alumno);
        return redirect()->to('alumnos');
    }
}
