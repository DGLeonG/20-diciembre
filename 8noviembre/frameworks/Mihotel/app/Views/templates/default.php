<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap4.css"/>
        <style>
            @import url(https://fonts.googleapis.com/css?family=Mountains+of+Christmas:700);

            body { 
                background-size:100%; 
                height:100vh;
                background-repeat: no-repeat;
                overflow:hidden;
                margin:0;
                width:100%;
            }
             
            h1 {
                background: #000854;
                background-repeat: no-repeat;
                background-position-x: left;
                background-position-y: top;
                background-size: 100px;
                font-size: 5.5vw;
                width:100%;
                height:100%;
                text-align:center;
                max-height:100px;
                z-index:-1;
                line-height:0;
                padding-top:60px;
                padding-bottom:60px;
                color:#fff;
                font-family: 'Arial', cursive;
            }
            
        </style>
        <title><?= $this->renderSection('head_title') ?></title>
    </head>
    <body>
        <div class="container" >
            <div class=""></div>
            <div class="title" id="">
                <h1 class="Listado Hoteles">
                    <?= $this->renderSection('title') ?>
                </h1>
            </div>
            <div class="main-content" id="">
                <?= $this->renderSection('content') ?>
            </div>
        </div>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap4.js"></script>
        <script type="text/javascript" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/191814/interact.min.js"></script>
    </body>
</html>

