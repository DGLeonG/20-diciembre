<?php

namespace App\Models;
use CodeIgniter\Model;


class AlumnosModel extends Model {
    
    protected $table = 'alumnos';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    protected $allowedFields = ['NIA','nombre','apellido1','apellido2'];
    
}

