<?= $this->extend('templates/default') ?>

<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>


    <table class="table table-striped" id="myTable">
        <thead>
            <th>
                Nombre
            </th>
            <th>
                Descripcion
            </th>
            <th>
                Tecnología
            </th>
            <th>
                Capacidad
            </th>
            <th>
                Adultos
            </th>
            <th>
                Ninyos
            </th>
        </thead>
        <tbody>
        <?php foreach ($habitaciones as $habitacion): ?>
            <tr>
                <td>
                    <?= $habitacion->nombre ?>
                </td>
                <td>
                    <?= $habitacion->descripcion ?>
                </td>
                <td>
                    <?= $habitacion->tecnologia ?>
                </td>
                <td>
                    <?= $habitacion->capacidad ?>
                </td>
                <td>
                    <?= $habitacion->adultos ?>
                </td>
                <td>
                    <?= $habitacion->ninyos ?>
                </td>
                <td class="text-right">
                    <a href=>
                    <span class="bi bi-pencil-square"></span>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>    
    </table>
<?= $this->endSection() ?>
