<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
        <title><?=$titulo?></title>
    </head>
    <body>
        <div class="container-fluid">
            <h1 class="text-primary"><?= $titulo?></h1>
            <table class="table table-striped">
                <?php foreach ($alumnos as $alumno): ?>
                    <tr>
                        <td>
                            <?= $alumno->NIA ?>
                        </td>
                        <td>
                            <?= $alumno->nombre ?> <?= $alumno->apellido1 ?> <?= $alumno->apellido2 ?>
                        </td>
                        <td>
                            <?= $alumno->email ?>
                        </td>
                        <td>
                            <a href="<?=site_url('alumnos/formedit/'.$alumno->id)?>" title="Editar <?= $alumno->nombre.' '.$alumno->apellido1 ?>">
                            <span class="bi bi-pen-fill text-primary"></span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </body>
</html>

