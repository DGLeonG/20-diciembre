<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?= $titulo?></title>
    </head>
    <body>
        <div class="container-fluid">
            <h1 class="text-primary"><?= $titulo?></h1>
            <?= form_open('grupos/actualiza/'.$grupo->id)?>
                <div class="form-group row">
                    <?=form_label('Codigo:', 'codigo', ['class'=>'col-sm-2 col-form-label'])?>
                    <div class="col-sm-10">
                        <?= form_input('codigo',$grupo->codigo,['class'=>'form_control', 'id'=>'codigo']) ?>
                    </div>
                </div>
                <div class="form-group row">
                    <?=form_label('Nombre:', 'nombre', ['class'=>'col-sm-2 col-form-label'])?>
                    <div class="col-sm-10">
                        <?= form_input('nombre',$grupo->nombre,['class'=>'form_control', 'id'=>'nombre']) ?>
                    </div>
                </div>
                
                  <div class="form-group row">
                    <div class="col-sm-10">
                      <?= form_submit('botoncito', 'Enviar', ['class'=>'btn btn-primary']) ?>
                    </div>
                  </div>

            <?= form_close()?>
        </div>
    </body>
</html>
