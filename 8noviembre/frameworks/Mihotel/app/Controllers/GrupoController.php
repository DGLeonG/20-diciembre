<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\GruposModel;

/**
 * Description of grupoController
 *
 * @author jbarrachinab
 */
class GrupoController extends BaseController {
    //put your code here
    public function index(){
        $grupoModel = new GruposModel();
        /*echo '<pre>';
        print_r($grupoModel->findAll());
        echo '</pre>';*/
        $data['titulo'] = 'Listado de Grupos';
        $data['grupos'] = $grupoModel->findAll();
        return view('grupos/lista',$data);
    }
    
    public function formEdit($id){
        helper('form');
        $grupoModel = new GruposModel();
        $data['titulo'] = 'Modificar Grupo';
        $data['grupo'] = $grupoModel->find($id);
        /*echo '<pre>';
        print_r($data);
        echo '</pre>';*/
        return view('grupos/formEdit',$data);
    }
    
    public function actualiza($id){
        //recoger datos
        $grupo = $this->request->getPost();
        unset($grupo['botoncito']);
        /*echo '<pre>';
        echo $id;
        print_r($grupo);
        echo '</pre>';*/
        $grupoModel = new GruposModel();
        $grupoModel->update($id,$grupo);
        return redirect()->to('grupos');
    }
}

