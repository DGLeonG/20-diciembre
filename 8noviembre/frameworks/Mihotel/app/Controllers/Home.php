<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function form(){
        $data['title'] = 'Formulario de Entrada';
        return view('alumnos/form', $data);
        /*
        echo '<form action="http://localhost:8080/codeigniter/index.php/alumnos" method="post">',"\n";
        echo '<label for="texto">Introduce el texto a buscar: </label>',"\n";
        echo '<input type="text" name="texto" value="Pon un valor" id="texto" />',"\n";
        echo '<input type="submit" name="enviar" value="Enviar" />',"\n";
        echo '</form>',"\n";
         * 
         */
    }
    
    public function formBuscaAlumnos(){
        helper('form');
        echo form_open('alumnos');
        echo form_label('Introduce el texto a buscar: ','texto');
        echo form_input('texto', 'Pon un valor', ['id'=>'texto', 'class'=>'bg-danger']);
        echo form_submit('enviar','Enviar');
        echo form_close();
    }
    
    public function muestraAlumnos(){
        $data['title'] = 'Listado de Alumnos';
        $alumnos = new \App\Models\Alumnos();
        $textoabuscar = $this->request->getPost('texto');
        $data['resultado'] = $alumnos->where(['apellido1'=>strtoupper($textoabuscar)])->findAll();
        /*echo "<pre>";
        print_r($resultado);
        echo "</pre>";*/
        return view('alumnos/lista',$data);
    }
    
    public function alumnosGrupo($grupo){
        $alumnos = new \App\Models\Alumnos();
        $data['resultado'] = $alumnos
                    ->select('alumnos.nombre, alumnos.apellido1, alumnos.apellido2')
                    ->join('matricula','matricula.NIA=alumnos.NIA','LEFT')
                    ->where(['matricula.grupo'=>$grupo])
                    ->findAll();
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }


}
