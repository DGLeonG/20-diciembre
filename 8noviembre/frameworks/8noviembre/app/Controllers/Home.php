<?php

namespace App\Controllers;
use App\Controllers\ProductoModel;

class Home extends BaseController
{
    public function mostrarProducto()
    {     
        $data['title'] = 'Listado Grupos';        
        
        $producto = new \App\Models\ProductoModel();
        
        $data['result'] = $producto
        ->select ('*')
        ->join ('familias as f','f.CodigoFamilia = productos.CodigoFamilia','left')
        ->findAll();
        
        return view('producto/lista',$data);
    }
    
}