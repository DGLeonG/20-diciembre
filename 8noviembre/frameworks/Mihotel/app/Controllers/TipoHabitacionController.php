<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\TipoHabitacionModel;

class TipoHabitacionController extends BaseController {
    
    public function muestraHab(){
        helper('form');
        $data['title'] = 'Listado de Habitaciones';
        $habitacionModel = new TipoHabitacionModel();
        $textoabuscar = $this->request->getPost('texto');
        $data['habitaciones'] = $habitacionModel->findAll();
        /*echo "<pre>";
        print_r($data['habitaciones']);
        echo "</pre>";*/
        return view('tipohabitacion/lista',$data);
    }
    
    
    public function insertHab(){
        $data['title'] = 'Alta Habitación';
        helper('form');
        if (strtolower($this->request->getMethod()) !== 'post') { //la primera vez
           return view('tipohabitacion/formAlta', $data); 
        } else {
            $alumno_nuevo = [
            'id' => $this->request->getPost('id'),
            'nombre' => $this->request->getPost('nombre'),
            'tecnologia' => $this->request->getPost('tecnologia'),
            'capacidad' => $this->request->getPost('capacidad'),
            'adultos' => $this->request->getPost('adultos'),
            'ninyos' => $this->request->getPost('ninyos'),
        ];
            $habitacionModel = new TipoHabitacionModel();
            if ($habitacionModel->insert($alumno_nuevo) === false){
               $data['errores'] = $habitacionModel->errors(); 
               return view('tipohabitacion/formAlta', $data);  
            }  
        }           
    }
}

