<?php

namespace App\Models;
use CodeIgniter\Model;

class ProModel extends Model {
    
    protected $table = 'productos';
    protected $primaryKey = 'CodigoProducto';
    protected $returnType = 'object';
    protected $allowedFields = ['codigoproducto', 'nombre', 'codigofamilia', 'caracteristicas', 'color', 'tipoiva'];
}